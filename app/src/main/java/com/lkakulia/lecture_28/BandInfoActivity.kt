package com.lkakulia.lecture_28

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_band_info.*
import org.json.JSONArray
import org.json.JSONObject

class BandInfoActivity : AppCompatActivity() {

    private lateinit var songsAdapter: SongRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_band_info)

        init()
    }

    private fun init() {
        val band = intent.getParcelableExtra<BandModel>("band")
        val bandName = band!!.name
        val bandInfo = band.info
        val bandImgUrl = band.imgUrl

        Glide.with(this)
            .load(bandImgUrl)
            .placeholder(R.mipmap.ic_launcher)
            .into(bandImageView)
        bandInfoTextView.text = bandInfo

        HttpRequest.getRequest(HttpRequest.SONG_TITLES, object: CustomCallback {
            override fun onFailure(error: String?) {
                Toast.makeText(this@BandInfoActivity, error, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(response: String) {
                val jsonArray = JSONObject(response).getJSONArray("data")

                (0 until jsonArray.length()).forEach {
                    val jsonBand = jsonArray.get(it) as JSONObject

                    if (jsonBand.getString("band") == bandName) {
                        val jsonSongs = jsonBand.getJSONArray("songs")

                        if (jsonSongs.length() > 0) {
                            (0 until jsonSongs.length()).forEach {
                                val jsonSong = jsonSongs.get(it)
                                val song = Gson().fromJson(
                                    jsonSong.toString(),
                                    BandModel.SongModel::class.java
                                )
                                band.songs.add(song)
                            }
                        }
                        else {
                            val song = BandModel.SongModel()
                            song.title = "Songs not found"
                            band.songs.add(song)
                        }
                    }
                }

                recyclerView.layoutManager = LinearLayoutManager(this@BandInfoActivity)
                songsAdapter = SongRecyclerViewAdapter(band.songs, object: CustomListener {
                    override fun onClick(position: Int) {
                        val intent = Intent(this@BandInfoActivity, SongLyricsActivity::class.java).apply {
                            putExtra("artist", bandName)
                            putExtra("title", band.songs[position].title)
                        }
                        startActivity(intent)
                    }
                })
                recyclerView.adapter = songsAdapter
            }

        })
    }
}

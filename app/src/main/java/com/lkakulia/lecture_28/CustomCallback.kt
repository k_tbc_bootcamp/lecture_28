package com.lkakulia.lecture_28

interface CustomCallback {
    fun onFailure(error: String?)
    fun onResponse(response: String)
}
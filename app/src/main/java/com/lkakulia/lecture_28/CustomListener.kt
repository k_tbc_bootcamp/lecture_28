package com.lkakulia.lecture_28

interface CustomListener {
    fun onClick(position: Int)
}
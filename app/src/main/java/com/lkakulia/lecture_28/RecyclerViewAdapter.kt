package com.lkakulia.lecture_28

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.recyclerview_item_layout.view.*

class RecyclerViewAdapter(
    private val bands: MutableList<BandModel>,
    private val customListener: CustomListener
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return bands.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var band: BandModel

        fun onBind() {
            band = bands[adapterPosition]

            itemView.setOnClickListener {
                customListener.onClick(adapterPosition)
            }
            Glide.with(itemView.context)
                .load(band.imgUrl)
                .placeholder(R.mipmap.ic_launcher)
                .into(itemView.bandImageView)
            itemView.bandTextView.text = band.name
        }
    }
}
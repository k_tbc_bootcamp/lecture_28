package com.lkakulia.lecture_28

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_song_lyrics.*
import org.json.JSONObject

class SongLyricsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_lyrics)

        init()
    }

    private fun init() {
        val artist = intent.getStringExtra("artist")
        val title = intent.getStringExtra("title")

        HttpRequest.getRequest(artist, title, object: CustomCallback {
            override fun onFailure(error: String?) {
                Toast.makeText(this@SongLyricsActivity, error, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(response: String) {
                val lyrics = JSONObject(response).getString("lyrics")
                songTitleTextView.text = title
                songLyricsTextView.text = lyrics
            }

        })
    }
}

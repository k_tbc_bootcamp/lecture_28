package com.lkakulia.lecture_28

import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.recyclerview_song_layout.view.*

class SongRecyclerViewAdapter(
    private val songs: MutableList<BandModel.SongModel>,
    private val customListener: CustomListener
) : RecyclerView.Adapter<SongRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_song_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return songs.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var song: BandModel.SongModel

        fun onBind() {
            song = songs[adapterPosition]

            if (song.title != "Songs not found") {
                itemView.setOnClickListener {
                    customListener.onClick(adapterPosition)
                }
            }
                itemView.titleTextView.text = song.title
        }
    }

}